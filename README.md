# This is BETA version of SilverOKNA API. #

This is example script for API services of SilverOKNA and it is responsible for UI generation on the client side.
API services are located on Production, Demo and Developing SilverOKNA servers.

API for developing server is located on adress:
http://dev.silverokna.pl/fbServices/SilverOKNADataService.svc/API

Please check our Wiki for API reference documentation.
Please report any issues on Issues site - but remember that this is still BETA version.

Best regards,
SilverOKNA Team