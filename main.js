﻿/*
------------------------------------------------------------------------------------- 
    SilverOKNA API
    ver 1.1
    Winkhaus Polska
------------------------------------------------------------------------------------- 
 */


function stepSelected(name, id, e) {
    this.name = name;
    this.id = id;
    this.e = e;
}


var domain = '{domain}';
var fbURL = '{fbURL}';


var actualStep = 1;
var objID = '00000000-0000-0000-0000-000000000000';
var BizAppURL = "";
var NIP = "";
var isBasket = false;
var basketItems = [];
var basketItemToDelete = "00000000-0000-0000-0000-000000000000";
var loading = false;
var callerID = "00000000-0000-0000-0000-000000000000";
var forceDBLanguage = null;
var step1SelectedObj = new stepSelected("", "", "");
var step2SelectedObj = new stepSelected("", "", "");
var step3SelectedObj = new stepSelected("", "", "");
var step4SelectedObj = new stepSelected("", "", "");
var step5SelectedObj = new stepSelected("", "", "");
var reCAPTCHAKey = "6LfFPxsUAAAAAGfmjVl6e5B8kbrAHVcqWyPXZnvp";


loadCSS("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css");
loadCSS(domain + "Content/css.ashx?id=" + document.getElementById("soapi").getAttribute("data-APIKey"));
loadScript("https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js", function () {
    loadScript("https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js", function () {
        StartPlugin();
    });
});


function StartPlugin() {
    $(document).ready(function () {

        callerID = document.getElementById("soapi").getAttribute("data-APIKey");
        forceDBLanguage = document.getElementById("soapi").getAttribute("data-DBLanguage");

        if (!String.prototype.format) {
            String.format = function () {
                var s = arguments[0];
                for (var i = 0; i < arguments.length - 1; i++) {
                    var reg = new RegExp("\\{" + i + "\\}", "gm");
                    s = s.replace(reg, arguments[i + 1]);
                }
                return s;
            }
        }

        PrepareHTMLEnvirnament();
    });
}

/* ------------------------------------------------------------------------------------- */
/* Helpers functions */
/* ------------------------------------------------------------------------------------- */
function loadCSS(url) {

    var css = document.createElement("link")

    css.setAttribute("rel", "stylesheet");
    css.setAttribute("href", url);
    

    if (css.readyState) { //IE
        css.onreadystatechange = function () {
            if (css.readyState == "loaded" || css.readyState == "complete") {
                css.onreadystatechange = null;
            }
        };
    } else { //Others
        css.onload = function () {
        };
    }

    document.getElementsByTagName("head")[0].appendChild(css);
}

function loadScript(url, callback) {

    var script = document.createElement("script")
    script.type = "text/javascript";

    if (script.readyState) { //IE
        script.onreadystatechange = function () {
            if (script.readyState == "loaded" || script.readyState == "complete") {
                script.onreadystatechange = null;
                callback();
            }
        };
    } else { //Others
        script.onload = function () {
            callback();
        };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

function getParameterByName(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function LoadingOn() {
    if ($("#items").children().length > 0)
        return;
    loading = true;
    $("#items").hide();
    $("#loading").fadeIn("slow", function () {
    });
}

function LoadingOff() {
    $("#loading").hide();
    $("#items").fadeIn("slow", function () {
    });
    loading = false;
}

function toColor(num) {
    num >>>= 0;
    var a = 1,
        r = (num & 0xFF0000) >>> 0,
        g = (num & 0xFF00) >>> 8,
        b = (num & 0xFF) >>> 16;

    return "rgba(" + [r, g, b, a].join(",") + ")";
}

function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
          .toString(16)
          .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
      s4() + '-' + s4() + s4() + s4();
}

function getBase64Image(img) {
    var data = '';
    $.ajax({
        async: false,
        cache: false,
        crossDomain: true,
        url: img,
        mimeType: "text/plain; charset=x-user-defined"
    }).done(function (data, textStatus, jqXHR) {
        step5SelectedObj.base64 = base64Encode(data);
    }).fail(function (jqXHR, textStatus, errorThrown) {
        alert("fail: " + errorThrown);
    });
    return data;
}

function base64Encode(str) {
    var CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    var out = "", i = 0, len = str.length, c1, c2, c3;
    while (i < len) {
        c1 = str.charCodeAt(i++) & 0xff;
        if (i == len) {
            out += CHARS.charAt(c1 >> 2);
            out += CHARS.charAt((c1 & 0x3) << 4);
            out += "==";
            break;
        }
        c2 = str.charCodeAt(i++);
        if (i == len) {
            out += CHARS.charAt(c1 >> 2);
            out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
            out += CHARS.charAt((c2 & 0xF) << 2);
            out += "=";
            break;
        }
        c3 = str.charCodeAt(i++);
        out += CHARS.charAt(c1 >> 2);
        out += CHARS.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
        out += CHARS.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
        out += CHARS.charAt(c3 & 0x3F);
    }
    return out;
}

function del(id) {
    basketItemToDelete = id;
}

function isNormalInteger(str) {
    var n = Math.floor(Number(str));
    return String(n) === str && n >= 0;
}

function Round(number) {
    return Math.round(number * 100) / 100;
}

/* ------------------------------------------------------------------------------------- */


function PrepareHTMLEnvirnament() {
    var env = ""
+ "    <div class=\"container-fluid so-header\">"
+ "        <div class=\"row so-center-row center-block\">"
+ "            <div class=\"col-sm-6 so-center\">"
+ "                <h5 id=\"header\" class=\"so-header-text\">Step 1 from 5</h5>"
+ "            </div>"
+ "            <div class=\"col-sm-6 so-center\">"
+ "                <a href=\"javascript:Basket();\"><img src=\"" + domain +"img/basket.png\" class=\"img-responsive pull-right\"/></a>"
+ "                <span class=\"badge pull-right\" id=\"badge\"></span>"
+ "            </div>"
+ "        </div>"
+ "    </div>"
+ "    <div class=\"container-fluid\" id=\"loading\">"
+ "        <img src=\"" + domain + "img/busy.gif\" class=\"img-responsive center-block\" />"
+ "    </div>"
+ "    <div class=\"container-fluid\" id=\"basket\">"
+ "    </div>"
+ "    <div class=\"container-fluid\" id=\"items\">"
+ "    </div>"
+ "    <div class=\"container-fluid so-footer\">"
+ "        <div class=\"container-fluid\">"
+ "            <div class=\"row\">"
+ "                <div class=\"col-sm-offset-6 col-sm-3 so-padding\">"
+ "                    <button type=\"button\" id=\"prevButton\" class=\"btn btn-default btn-block\">" + dict("PreviousButtonText") + "</button>"
+ "                </div>"
+ "                <div class=\"col-sm-3 so-padding\">"
+ "                    <button type=\"button\" id=\"nextButton\" class=\"btn btn-default btn-block\">" + dict("NextButtonText") + "</button>"
+ "                </div>"
+ "            </div>"
+ "        </div>"
+ "        <div id=\"orderForm\"></div>"
+ "    </div>"
+ "    <script src=\"https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit\" async defer></script>";

    $("#soapi").parent().append(env);

    FormModal();

    $.ajax({
        url: fbURL + '/GetAPIParameters',
        method: "POST",
        data: "{ \"APIKey\": \"" + callerID + "\", \"ForceDBLanguage\": \"" + forceDBLanguage + "\" }",
        async: true,
        cache: false,
        crossDomain: true,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json'
    })
    .done(function (data) {
        objID = data.d.APIKey;
        BizAppURL = data.d.BizAppURL;
        NIP = data.d.NIP;

        if (actualStep === 1)
            GetStep1Items();
    });

}

function Basket() {
    if (loading)
        return;

    if (isBasket)
    {
        //display steps
        $("#basket").fadeOut("slow", function () {
            $("#items").removeClass('so-hide');
        });
        $("#header").text(String.format(dict("HeaderText"), actualStep));
        $("#prevButton").show();
        $("#nextButton").text(dict("NextButtonText"));
        if ($(".so-button.so-selected").length == 0)
            $("#nextButton").attr("disabled", true);
        else {
            $("#nextButton").attr("disabled", false);
        }
        //Bind nextButton to next step
        $("#nextButton").unbind();
        $("#nextButton").show();
        $('#nextButton').click(function (e) {
            var currentStepNumber = actualStep + 1;
            eval("GetStep" + currentStepNumber + "Items();");
        });
        isBasket = !isBasket;
    }
    else
    {
        //display basket
        $("#items").addClass('so-hide');
        $("#basket").fadeIn("slow", function () {
        });
        isBasket = !isBasket;
        $("#header").text(dict("MyBasketHeaderText"));
        FillBasket();
    }
}

function FillBasket() {
    $("#basket").empty();
    if (basketItems.length == 0) {

        var infoBody = ""
        + "<div id=\"infoText\"><br/><div class=\"alert alert-info\">"
        + "    <strong>" + dict("MyBasketInfoHeaderText") + "</strong> " + dict("MyBasketInfoBodyText")
        + "</div></div>";

        $("#basket").append(infoBody);
        return;
    }
    
    var sum = 0;
    var currency = "";
    var basketBody = "<div class=\"list-group\" style=\"padding: 10px\">";
     
    for (var i = 0; i < basketItems.length; i++) {

        var priceText = "<img id=\"loading1_" + i + "\" src=\"" + domain + "/img/loading.gif\" />";
        var ammountText = "<img id=\"loading2_" + i + "\" src=\"" + domain + "/img/loading.gif\" />";

        if (basketItems[i].price > 0)
        {
            currency = basketItems[i].currency;
            priceText = Round(basketItems[i].price) + " " + basketItems[i].currency;
            ammountText = Round(basketItems[i].quantity * basketItems[i].price) + " " + currency;
            sum += Round(basketItems[i].quantity * basketItems[i].price);
        }
        else if (basketItems[i].price == -1)
        {
            //Construction warning
            priceText = "<span class=\"label label-warning\">" + dict("VerificationFail") + "</span>";
            ammountText = "<span class=\"label label-warning\">" + dict("VerificationFail") + "</span>";
        }

        var row = ""
        + "<div class=\"list-group-item\">"
        + "    <div class=\"row\">"
        + "        <div class=\"col-sm-3\">"
        + "            <img class=\"img-responsive center-block\" src=\"data:image/png;charset=utf-8;base64," + basketItems[i].base64 + "\"/>"
        + "            <br/>"
        + "        </div>"
        + "        <div class=\"col-sm-9\">"
        + "            <div class=\"table-responsive\">"
        + "                <table class=\"table table-condensed\">"
        + "                    <thead>"
        + "                        <tr>"
        + "                            <th class=\"text-right col-sm-2\">" + dict("MyBasketItemProp1") + "</th>"
        + "                            <th class=\"text-left col-sm-9\" colspan=\"4\">" + basketItems[i].type + "</th>"
        + "                        </tr>"
        + "                    </thead>"
        + "                    <tbody>"
        + "                        <tr>"
        + "                            <td class=\"text-right col-sm-3\">" + dict("MyBasketItemProp2") + "</td>"
        + "                            <td class=\"text-left col-sm-9\">" + basketItems[i].quantity + "</td>"
        + "                        </tr>"
        + "                        <tr>"
        + "                            <td class=\"text-right col-sm-3\">" + dict("MyBasketItemProp3") + "</td>"
        + "                            <td class=\"text-left col-sm-9\">" + basketItems[i].color + "</td>"
        + "                        </tr>"
        + "                        <tr>"
        + "                            <td class=\"text-right col-sm-3\">" + dict("MyBasketItemProp4") + "</td>"
        + "                            <td class=\"text-left col-sm-9\">" + basketItems[i].glass + "</td>"
        + "                        </tr>"
        + "                        <tr>"
        + "                            <td class=\"text-right col-sm-3\">" + dict("MyBasketItemProp5") + "</td>"
        + "                            <td class=\"text-left col-sm-9\">" + basketItems[i].fitting + "</td>"
        + "                        </tr>"
        + "                        <tr>"
        + "                            <td class=\"text-right col-sm-3\">" + dict("MyBasketItemProp6") + "</td>"
        + "                            <td class=\"text-left col-sm-9\">" + basketItems[i].width + "</td>"
        + "                        </tr>"
        + "                        <tr>"
        + "                            <td class=\"text-right col-sm-3\">" + dict("MyBasketItemProp7") + "</td>"
        + "                            <td class=\"text-left col-sm-9\">" + basketItems[i].height + "</td>"
        + "                        </tr>"
        + "                        <tr>"
        + "                            <td class=\"text-right col-sm-3\" id=\"priceLabel1_" + i + "\">" + dict("MyBasketItemProp8") + "</td>"
        + "                            <td class=\"text-left col-sm-9\" id=\"price1_" + i + "\">" + priceText + "</td>"
        + "                        </tr>"
        + "                        <tr>"
        + "                            <td class=\"text-right col-sm-3\">" + dict("MyBasketItemProp9") + "</td>"
        + "                            <td class=\"text-left col-sm-9\" id=\"price2_" + i + "\">" + ammountText + "</td>"
        + "                        </tr>"
        + "                        <tr>"
        + "                            <td class=\"text-right col-sm-3\">" + dict("MyBasketItemProp10") + "</td>"
        + "                            <td class=\"text-left col-sm-9\" id=\"price2_" + i + "\"><textarea class=\"form-control\" rows=\"5\" id=\"note_" + i + "\">" + basketItems[i].note + "</textarea></td>"
        + "                        </tr>"
        + "                        <tr>"
        + "                            <td colspan=\"2\" class=\"text-right col-sm-12\"><a href=\"#\" class=\"btn btn-primary\" id=\"" + basketItems[i].id + "\" data-toggle=\"modal\" data-target=\"#myModal\"><span class=\"glyphicon glyphicon-trash\"></span>&nbsp;" + dict("Delete") + "</a></td>"
        + "                        </tr>"
        + "                    </tbody>"
        + "                </table>"
        + "            </div>"
        + "        </div>"
        + "    </div>"
        + "</div>";

        basketBody += row;
    }
    basketBody += "     <div class=\"row\">"
    basketBody += "         <div class=\"col-sm-offset-3 col-sm-9\">"
    basketBody += "             <h2 id=\"sum1\">" + dict("Sum") + " " + Round(sum) + " " + currency +"</h2>"
    basketBody += "         </div>"
    basketBody += "     </div>"
    basketBody += "</div>"

    var modal = ""
    + "<div id=\"myModal\" class=\"modal fade\" role=\"dialog\">"
    + "  <div class=\"modal-dialog\">"
    + "    <div class=\"modal-content\">"
    + "      <div class=\"modal-header so-modal-header\">"
    + "        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>"
    + "        <h4 class=\"modal-title\">" + dict("MyBasketConfirmHeaderText") + "</h4>"
    + "      </div>"
    + "      <div class=\"modal-body\">"
    + "        <p>" + dict("MyBasketConfirmBodyText") + "</p>"
    + "      </div>"
    + "      <div class=\"modal-footer\">"
    + "        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" id=\"deleteYes\">" + dict("YesDelete") + "</button>"
    + "        <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">" + dict("Cancel") + "</button>"
    + "      </div>"
    + "    </div>"
    + "  </div>"
    + "</div>";
    basketBody += modal;



    $("#basket").append(basketBody);
    $('a.btn, a.btn-primary').click(function (e) {
        del(this.id);
    });
    $('textarea.form-control').on('input', function (e) {
        if (this.id.substring(0,5) != "note_")
            return;
        var i = this.id.replace("note_", "");
        basketItems[i].note = $("#" + this.id).val();
    });
    $("#deleteYes").click(function () {
        var result = $.grep(basketItems, function (e) { return e.id == basketItemToDelete; });
        if (result.length == 1) {
            basketItems.splice(result[0], 1);
            $('#myModal').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            if (basketItems.length == 0)
                $("#badge").text("");
            else
                $("#badge").text(basketItems.length);
            FillBasket();
        }
    });

    for (var i = 0; i < basketItems.length; i++) {
        var bitem = basketItems[i];
        if (bitem.price == 0) {
            var url = fbURL + "/GetBasketInfo";
            $.ajax({
                url: url,
                method: "POST",
                async: true,
                cache: false,
                crossDomain: true,
                data: "{ \"id\" : \"" + bitem.id + "\", \"type\" : \"" + bitem.idtype + "\", \"color\" : \"" + bitem.idcolor + "\", \"glazing\" : \"" + bitem.idglass + "\", \"fittingsCategory\" : \"" + bitem.idfitting + "\", \"construction\" : \"" + bitem.idconstruction + "\", \"width\" : \"" + bitem.width + "\", \"height\" : \"" + bitem.height + "\", \"keyID\": \"" + objID + "\", \"quantity\": \"" + bitem.quantity + "\"}",
                contentType: 'application/json; charset=utf-8',
                basketObj: bitem,
                customID: i,
                dataType: 'json'
            })
            .done(function (data, text, res) {
                var obj = data.d;
                var basketItem = this.basketObj;
                var it = this.customID;

                basketItem.price = Round(obj.price);
                basketItem.currency = obj.currency;;

                if (basketItem.price > 0)
                {
                    sum += Round(basketItem.price * basketItem.quantity);
                    $("#loading1_" + it).remove();
                    $("#price1_" + it).append(Round(basketItem.price) + " " + basketItem.currency);
                    $("#loading2_" + it).remove();
                    $("#price2_" + it).append(Round(basketItem.quantity * basketItem.price) + " " + basketItem.currency);
                    $("#sum1").empty();
                    $("#sum1").append(dict("Sum") + " " + Round(sum) + " " + basketItem.currency);
                    if (obj.FieldName.length > 0)
                    {
                        $("#priceLabel1_" + it).empty();
                        $("#priceLabel1_" + it).append(obj.FieldName + ":");
                    }
                }
                else
                {
                    $("#loading1_" + it).remove();
                    $("#loading2_" + it).remove();
                    $("#price1_" + it).append("<span class=\"label label-warning\">" + dict("VerificationFail") + "</span>");
                    $("#price2_" + it).append("<span class=\"label label-warning\">" + dict("VerificationFail") + "</span>");
                }
            });
        }
    }

    if (basketItems.length > 0) {
        $("#prevButton").hide();
        $('#nextButton').unbind();
        $("#nextButton").text(dict("SendOfferButtonText"));
        $("#nextButton").attr("disabled", false);
        $('#nextButton').click(function (e) {
            $('#myModal_OrderForm').modal('show');
        });
    }
}

///Type
function GetStep1Items() {
    $("#items").empty();
    LoadingOn();
    actualStep = 1;
    $("#header").text(String.format(dict("HeaderText"), actualStep));
    var url = fbURL + "/GetStep1Items";
    $.ajax({
        url: url,
        method: "POST",
        async: true,
        cache: false,
        crossDomain: true,
        data: "{ \"keyID\": \"" + objID + "\" }",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json'
    })
    .done(function (data) {
        var i = 0,
            d = 0,
            obj = data.d;
        $("#items").append("<div id=\"row" + d + "\" class=\"row\"></div>");
        $.each(obj, function (index, item) {
            i++;
            if (item.id == -1)
            {
                $("#items").empty();
                $("#items").append("<br/><div class=\"alert alert-danger\"><strong>Error!</strong> " + item.description + "</div>");
                $('#nextButton').hide();
                $('#prevButton').hide();
                return;
            }

            if (i === 4) {
                d++;
                $("#items").append("<div id=\"row" + d + "\" class=\"row\"></div>");
                i = 1;
            }
            var greenCss = "",
                boxHidden = "";
            if (item.id == step1SelectedObj.id)
                greenCss = " so-selected";
            else
                boxHidden = " so-box-hidden";
            $("#row" + d).append(""
+ "<div class=\"col-sm-4 so-padding\">"
+ "    <div class=\"so-button" + greenCss + "\">"
+ "        <h1><span class=\"glyphicon glyphicon-ok-sign so-box" + boxHidden + "\"></span></h1>"
+ "        <div class=\"row\">"
+ "            <div class=\"col-sm-12\" id=\"content_" + index + "\">"
+ "                 <img class=\"img-responsive center-block so-img-box\" src=\"" + item.imgUrl + "\" alt=\"Picture 1\">"
+ "             </div>"
+ "         </div>"
+ "        <h4>" + item.name + "</h4>"
+ "        <div>" + item.description + "</div>"
+ "        <input type=\"hidden\" id=\"e\" value=\"" + item.e + "\"/>"
+ "        <input type=\"hidden\" id=\"id\" value=\"" + item.id + "\"/>"
+ "        <input type=\"hidden\" id=\"name\" value=\"" + item.name + "\"/>"
+ "     </div>"
+ "</div>");
            if (i < 3)
                $("#row" + d).append("<div class=\"col-md-offset-" + (3 - i) + "\"></div>");
        });
        $('.so-button').click(function () {
            $(".so-selected").removeClass("so-selected");
            $(this).addClass('so-selected');
            $(".glyphicon-ok-sign").addClass('so-box-hidden');
            $(".glyphicon-ok-sign", this).removeClass('so-box-hidden');
            step1SelectedObj.e = $("#e", this)[0].value;
            step1SelectedObj.id = $("#id", this)[0].value;
            step1SelectedObj.name = $("#name", this)[0].value;
            $("#nextButton").attr("disabled", false);
        });
        LoadingOff();        
    })
    .always(function() {
        if ($(".so-button.so-selected").length == 0)
            $("#nextButton").attr("disabled", true);
        else {
            $("#nextButton").attr("disabled", false);
        }
    });
    $('#nextButton').unbind();
    $('#prevButton').unbind();
    $("#prevButton").attr("disabled", true);
    $("#nextButton").click(function () {
        GetStep2Items();
    });
}

///Color
function GetStep2Items() {
    $("#items").empty();
    LoadingOn();
    actualStep = 2;
    $("#header").text(String.format(dict("HeaderText"), actualStep));
    var url = fbURL + "/GetStep2Items";
    $.ajax({
        url: url,
        method: "POST",
        async: true,
        cache: false,
        crossDomain: true,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: "{ \"type\" : \"" + step1SelectedObj.e + "\", \"keyID\": \"" + objID + "\" }"
    })
    .done(function (data) {
        var i = 0,
            d = 0,
            obj = data.d;
        $("#items").append("<div id=\"row" + d + "\" class=\"row\"></div>");
        $.each(obj, function (index, item) {
            i++;
            if (i === 4) {
                d++;
                $("#items").append("<div id=\"row" + d + "\" class=\"row\"></div>");
                i = 1;
            }
            var greenCss = "",
                boxHidden = "";
            if (item.id == step2SelectedObj.id)
                greenCss = " so-selected";
            else
                boxHidden = " so-box-hidden";

            $("#row" + d).append(""
+ "<div class=\"col-sm-4 so-padding\">"
+ "    <div class=\"so-button" + greenCss + "\">"
+ "        <h1><span class=\"glyphicon glyphicon-ok-sign so-box" + boxHidden + "\"></span></h1>"
+ "        <div class=\"row\">"
+ "            <div class=\"col-sm-12\" id=\"content_" + index + "\">"
+ "            </div>"
+ "        </div>"
+ "        <div class=\"row\">"
+ "            <div class=\"col-sm-12\">"
+ "                <h6 class=\"text-center\">" + item.name + "</h6>"
+ "            </div>"
+ "        </div>"
+ "        <input type=\"hidden\" id=\"e\" value=\"" + item.e + "\"/>"
+ "        <input type=\"hidden\" id=\"id\" value=\"" + item.id + "\"/>"
+ "        <input type=\"hidden\" id=\"name\" value=\"" + item.name + "\"/>"
+ "     </div>"
+ "</div>");

            $.ajax({
                type: "GET",
                async: true,
                cache: false,
                indexValue: index,
                crossDomain: true,
                url: domain + "Content/color.ashx?id=" + item.id + "&keyID=" + objID + "&NIP=" + NIP + "&URL=" + BizAppURL,
                contentType: "image/png"
            })
            .done(function (dataImg) {
                if (dataImg.length)
                    $("#content_" + this.indexValue).append($("<img class=\"img-responsive center-block so-img-box so-img-color-box\" src=\"" + domain + "Content/color.ashx?id=" + item.id + "&keyID=" + objID + "&NIP=" + NIP + "&URL=" + BizAppURL + "\" alt=\"Picture 1\">"));
                else
                    $("#content_" + this.indexValue).append($("<div class=\"so-color-box center-block\" style=\"background-color: " + toColor(item.r) + "\"><br/><br/><br/><br/><br/></div>"));
            })
            .fail(function (error, txtStatus) {
                $("#content_" + this.indexValue).append($("<div class=\"so-color-box center-block\" style=\"background-color: " + toColor(item.r) + "\"><br/><br/><br/><br/><br/></div>"));
            })
            .always(function() {
                if ($(".so-button.so-selected").length == 0)
                    $("#nextButton").attr("disabled", true);
                else {
                    $("#nextButton").attr("disabled", false);
                }
            });


            if (i < 3)
                $("#row" + d).append("<div class=\"col-md-offset-" + (3 - i) + "\"></div>");
        });
        $('.so-button').click(function () {
            $(".so-selected").removeClass("so-selected");
            $(this).addClass('so-selected');
            $(".glyphicon-ok-sign").addClass('so-box-hidden');
            $(".glyphicon-ok-sign", this).removeClass('so-box-hidden');
            step2SelectedObj.e = $("#e", this)[0].value;
            step2SelectedObj.id = $("#id", this)[0].value;
            step2SelectedObj.name = $("#name", this)[0].value;
            $("#nextButton").attr("disabled", false);
        });
        LoadingOff();
    })
    .always(function () {
        if ($(".so-button.so-selected").length == 0)
            $("#nextButton").attr("disabled", true);
        else {
            $("#nextButton").attr("disabled", false);
        }
    });

    $('#prevButton').unbind();
    $('#nextButton').unbind();
    $("#prevButton").attr("disabled", false);
    $('#prevButton').click(function () {
        GetStep1Items();
    });
    $("#nextButton").click(function () {
        GetStep3Items();
    });
    $("#nextButton").attr("disabled", true);
}

///Glass
function GetStep3Items() {
    $("#items").empty();
    LoadingOn();
    actualStep = 3;
    $("#header").text(String.format(dict("HeaderText"), actualStep));
    var url = fbURL + "/GetStep3Items";
    $.ajax({
        url: url,
        method: "POST",
        async: true,
        cache: false,
        crossDomain: true,
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: "{ \"type\" : \"" + step1SelectedObj.e + "\", \"keyID\": \"" + objID + "\" }"        
    })
    .done(function (data) {
        var i = 0,
            d = 0,
            obj = data.d;
        $("#items").append("<div id=\"row" + d + "\" class=\"row\"></div>");
        $.each(obj, function (index, item) {
            i++;
            if (i === 4) {
                d++;
                $("#items").append("<div id=\"row" + d + "\" class=\"row\"></div>");
                i = 1;
            }
            var greenCss = "",
                boxHidden = "";
            if (item.id == step3SelectedObj.id)
                greenCss = " so-selected";
            else
                boxHidden = " so-box-hidden";
            $("#row" + d).append(""
+ "<div class=\"col-sm-4 so-padding\">"
+ "    <div class=\"so-button" + greenCss + "\" data-toggle=\"modal\" data-target=\"#myModal_" + index + "\">"
+ "         <h1><span class=\"glyphicon glyphicon-ok-sign so-box" + boxHidden + "\"></span></h1>"
+ "         <img class=\"img-responsive center-block so-img-box\" src=\"" + item.imgUrl + "\" alt=\"Picture 1\">"
+ "         <h4 class=\"text-center\">" + item.name + "</h4>"
+ "         <input type=\"hidden\" id=\"e\" value=\"" + item.e + "\"/>"
+ "         <input type=\"hidden\" id=\"id\" value=\"" + item.id + "\"/>"
+ "         <input type=\"hidden\" id=\"name\" value=\"" + item.name + "\"/>"
+ "     </div>"
+ "     <div id=\"myModal_" + index + "\" class=\"modal fade\" role=\"dialog\">"
+ "         <div class=\"modal-dialog\">"
+ "             <div class=\"modal-content\">"
+ "                 <div class=\"modal-header so-modal-header\">"
+ "                     <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>"
+ "                     <h4 class=\"modal-title\">" + item.name + "</h4>"
+ "                 </div>"
+ "                 <div class=\"modal-body\">"
+ "                     <p>" + item.description + "</p>"
+ "                 </div>"
+ "                 <div class=\"modal-footer\">"
+ "                     <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">" + dict("CloseButtonText") + "</button>"
+ "                 </div>"
+ "             </div>"
+ "         </div>"
+ "    </div>"
+ "</div>");
            if (i < 3)
                $("#row" + d).append("<div class=\"col-md-offset-" + (3 - i) + "\"></div>");
        });
        $('.so-button').click(function () {
            $(".so-selected").removeClass("so-selected");
            $(this).addClass('so-selected');
            $(".glyphicon-ok-sign").addClass('so-box-hidden');
            $(".glyphicon-ok-sign", this).removeClass('so-box-hidden');
            step3SelectedObj.e = $("#e", this)[0].value;
            step3SelectedObj.id = $("#id", this)[0].value;
            step3SelectedObj.name = $("#name", this)[0].value;
            $("#nextButton").attr("disabled", false);
        });
        LoadingOff();
    })
    .always(function () {
        if ($(".so-button.so-selected").length == 0)
            $("#nextButton").attr("disabled", true);
        else {
            $("#nextButton").attr("disabled", false);
        }
    });

    $('#nextButton').unbind();
    $('#prevButton').unbind();
    $("#prevButton").attr("disabled", false);
    $('#prevButton').click(function () {
        GetStep2Items();
    });
    $("#nextButton").click(function () {
        GetStep4Items();
    });
    $("#nextButton").attr("disabled", true);
    
}

///Hardware
function GetStep4Items() {
    $("#items").empty();
    LoadingOn();
    actualStep = 4;
    $("#header").text(String.format(dict("HeaderText"), actualStep));
    var url = fbURL + "/GetStep4Items";
    $.ajax({
        url: url,
        method: "POST",
        async: true,
        cache: false,
        crossDomain: true,
        data: "{ \"type\" : \"" + step1SelectedObj.e + "\", \"keyID\": \"" + objID + "\" }",
        contentType: 'application/json; charset=utf-8',
        dataType: 'json'
    })
    .done(function (data) {
        var i = 0,
            d = 0,
            obj = data.d;
        $("#items").append("<div id=\"row" + d + "\" class=\"row\"></div>");
        $.each(obj, function (index, item) {
            i++;
            if (i === 4) {
                d++;
                $("#items").append("<div id=\"row" + d + "\" class=\"row\"></div>");
                i = 1;
            }
            var greenCss = "",
                boxHidden = "";
            if (item.id == step4SelectedObj.id)
                greenCss = " so-selected";
            else
                boxHidden = " so-box-hidden";
            $("#row" + d).append(""
+ "<div class=\"col-sm-4 so-padding\">"
+ "    <div class=\"so-button" + greenCss + "\">"
+ "        <h1><span class=\"glyphicon glyphicon-ok-sign so-box" + boxHidden + "\"></span></h1>"
+ "        <img class=\"img-responsive center-block so-img-box\" src=\"" + item.imgUrl + "\" alt=\"Picture 1\">"
+ "        <h4>" + item.name + "</h4>"
+ "        <div>" + item.description + "</div>"
+ "        <input type=\"hidden\" id=\"e\" value=\"" + item.e + "\"/>"
+ "        <input type=\"hidden\" id=\"id\" value=\"" + item.id + "\"/>"
+ "        <input type=\"hidden\" id=\"name\" value=\"" + item.name + "\"/>"
+ "     </div>"
+ "</div>");
            if (i < 3)
                $("#row" + d).append("<div class=\"col-md-offset-" + (3 - i) + "\"></div>");
        });
        $('.so-button').click(function () {
            $(".so-selected").removeClass("so-selected");
            $(this).addClass('so-selected');
            $(".glyphicon-ok-sign").addClass('so-box-hidden');
            $(".glyphicon-ok-sign", this).removeClass('so-box-hidden');
            step4SelectedObj.e = $("#e", this)[0].value;
            step4SelectedObj.id = $("#id", this)[0].value;
            step4SelectedObj.name = $("#name", this)[0].value;
            $("#nextButton").attr("disabled", false);
        });
        LoadingOff();
    })
    .always(function () {
        if ($(".so-button.so-selected").length == 0)
            $("#nextButton").attr("disabled", true);
        else {
            $("#nextButton").attr("disabled", false);
        }
    });
    $('#nextButton').unbind();
    $('#prevButton').unbind();
    $("#prevButton").attr("disabled", false);
    $('#prevButton').click(function () {
        GetStep3Items();
    });
    $("#nextButton").click(function () {
        GetStep5Items();
    });
    $("#nextButton").attr("disabled", true);
}

///Construction
function GetStep5Items() {
    $("#items").empty();
    LoadingOn();
    actualStep = 5;
    $("#header").text(String.format(dict("HeaderText"), actualStep));
    var url = fbURL + "/GetStep5Items";
    $.ajax({
        url: url,
        method: "POST",
        async: true,
        cache: false,
        crossDomain: true,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: "{ \"type\" : \"" + step1SelectedObj.e + "\", \"color\" : \"" + step2SelectedObj.id + "\", \"glazing\" : \"" + step3SelectedObj.e + "\", \"fittingsCategory\" : \"" + step4SelectedObj.e + "\", \"keyID\": \"" + objID + "\"}"
    })
    .done(function (data) {
        var i = 0,
            d = 0,
            obj = data.d;
        $("#items").append("<div id=\"row" + d + "\" class=\"row\"></div>");

        $.each(obj, function (index, item) {
            i++;
            if (i === 4) {
                d++;
                $("#items").append("<div id=\"row" + d + "\" class=\"row\"></div>");
                i = 1;
            }
            var greenCss = "",
                boxHidden = "";
            if (index == step5SelectedObj.value)
                greenCss = " so-selected";
            else
                boxHidden = " so-box-hidden";

            $("#row" + d).append(""
+ "<div class=\"col-sm-4 so-padding\">"
+ "    <div class=\"so-button" + greenCss + "\">"
+ "        <h1><span class=\"glyphicon glyphicon-ok-sign so-box" + boxHidden + "\"></span></h1>"
+ "        <div class=\"row\">"
+ "            <div class=\"col-sm-12\" id=\"content_" + index + "\">"
+ "                <img id=\"img_" + index + "\" class=\"img-responsive center-block so-img-box\" src=\"" + item.imgUrl + "\" alt=\"Picture 1\">"
+ "            </div>"
+ "        </div>"
+ "        <h4>" + item.name + "</h4>"
+ "        <div>" + item.description + "</div>"
+ "        <input type=\"hidden\" id=\"id\" value=\"" + item.id + "\"/>"
+ "        <input type=\"hidden\" id=\"e\" value=\"" + item.halfUrl + "\"/>"
+ "        <input type=\"hidden\" id=\"value\" value=\"" + index + "\"/>"
+ "     </div>"
+ "</div>");

            if (i < 3)
                $("#row" + d).append("<div class=\"col-md-offset-" + (3 - i) + "\"></div>");
        });
        $('.so-button').click(function () {
            $(".so-selected").removeClass("so-selected");
            $(this).addClass('so-selected');
            $(".glyphicon-ok-sign").addClass('so-box-hidden');
            $(".glyphicon-ok-sign", this).removeClass('so-box-hidden');
            step5SelectedObj.id = $("#id", this)[0].value;
            step5SelectedObj.value = $("#value", this)[0].value;
            getBase64Image($("#e", this)[0].value);
            step5SelectedObj.e = $("#e", this)[0].value;
            $("#nextButton").attr("disabled", false);
        });
        LoadingOff();
    })
    .always(function () {
        if ($(".so-button.so-selected").length == 0)
            $("#nextButton").attr("disabled", true);
        else {
            $("#nextButton").attr("disabled", false);
        }
    });

    $('#nextButton').unbind();
    $('#prevButton').unbind();
    $("#prevButton").attr("disabled", false);
    $('#prevButton').click(function () {
        GetStep4Items();
    });
    $("#nextButton").click(function () {
        GetStep6Items();
    });
    $("#nextButton").attr("disabled", true);
}

///Dimensions
function GetStep6Items() {
    $("#items").empty();
    $('#nextButton').unbind();
    $('#prevButton').unbind();
    actualStep = 5;
    $("#header").text(String.format(dict("HeaderText"), actualStep));
    $("#nextButton").text(dict("AddButtonText"));

    var sizeBody = ""
    + "<form class=\"form-horizontal\">"
    + "    <div class=\"container-fluid\">"
    + "        <div class=\"row\">"
    + "            <div class=\"col-sm-12\">"
    + "                <br/>"
    + "                <img class=\"img-responsive center-block\" src=\"" + step5SelectedObj.e + "&w=400&h=400\" />"
    + "                <br/>"
    + "            </div>"
    + "        </div>"
    + "        <div class=\"row\">"
    + "            <div class=\"class=\"col-sm-6\">"
    + "                <div class=\"row\">"
    + "                    <div class=\"col-xs-6\">"
    + "                        <p class=\"text-right\"><strong>" + dict("MyBasketItemProp1") + "</strong></p>"
    + "                    </div>"
    + "                    <div class=\"col-xs-6\">"
    + "                        <p><strong>" + step1SelectedObj.name + "</strong></p>"
    + "                    </div>"
    + "                </div>"
    + "                <div class=\"row\">"
    + "                    <div class=\"col-xs-6\">"
    + "                        <p class=\"text-right\">" + dict("MyBasketItemProp3") + "</p>"
    + "                    </div>"
    + "                    <div class=\"col-xs-6\">"
    + "                        <p>" + step2SelectedObj.name + "</p>"
    + "                    </div>"
    + "                </div>"
    + "                <div class=\"row\">"
    + "                    <div class=\"col-xs-6\">"
    + "                        <p class=\"text-right\">" + dict("MyBasketItemProp4") + "</p>"
    + "                    </div>"
    + "                    <div class=\"col-xs-6\">"
    + "                        <p>" + step3SelectedObj.name + "</td>"
    + "                    </div>"
    + "                </div>"
    + "                <div class=\"row\">"
    + "                    <div class=\"col-xs-6\">"
    + "                        <p class=\"text-right\">" + dict("MyBasketItemProp5") + "</p>"
    + "                    </div>"
    + "                    <div class=\"col-xs-6\">"
    + "                        <p>" + step4SelectedObj.name + "</td>"
    + "                    </div>"
    + "                </div>"
    + "            </div>"
    + "        </div>"
    + "        <br/>"
    + "        <div class=\"row\">"
    + "            <div class=\"text-left col-sm-12\">"
    + "                <div id=\"input1subdiv\" class=\"form-group\">"
    + "                    <label for=\"input1\" class=\"col-sm-2 control-label\">" + dict("MyBasketItemProp2") + "</label>"
    + "                    <div class=\"col-sm-10\">"
    + "                        <input type=\"number\" id=\"input1\" class=\"form-control\" placeholder=\"" + dict("MyBasketItemProp2").replace(":", "").toLowerCase() + "\">"
    + "                    </div>"
    + "                </div>"
    + "            </div>"
    + "            <div id=\"input2div\" class=\"text-left col-sm-12\">"
    + "                <div id=\"input2subdiv\" class=\"form-group\">"
    + "                    <label for=\"input2\" class=\"col-sm-2 control-label\">" + dict("MyBasketItemProp6") + "</label>"
    + "                    <div class=\"col-sm-10\">"
    + "                        <input type=\"number\" id=\"input2\" class=\"form-control\" placeholder=\"" + dict("MyBasketItemProp6").replace(":", "").toLowerCase() + "\">"
    + "                    </div>"
    + "                </div>"
    + "            </div>"
    + "            <div id=\"input3div\" class=\"text-left col-sm-12\">"
    + "                <div id=\"input3subdiv\" class=\"form-group\">"
    + "                    <label for=\"input3\" class=\"col-sm-2 control-label\">" + dict("MyBasketItemProp7") + "</label>"
    + "                    <div class=\"col-sm-10\">"
    + "                        <input type=\"number\" id=\"input3\" class=\"form-control\" placeholder=\"" + dict("MyBasketItemProp7").replace(":", "").toLowerCase() + "\">"
    + "                    </div>"
    + "                </div>"
    + "            </div>"
    + "        </div>"
    + "    </div>"
    + "</form><br/><br/>";

    $("#items").append(sizeBody);

    $("#input1").on('input', function (e) {
        if (isNormalInteger($("#input1").val()) != false)
            $("#input1subdiv").removeClass("has-error");
        else
            $("#input1subdiv").addClass("has-error");
    });
    $("#input2").on('input', function (e) {
        if (isNormalInteger($("#input2").val()) != false)
            $("#input2subdiv").removeClass("has-error");
        else
            $("#input2subdiv").addClass("has-error");
    });
    $("#input3").on('input', function (e) {
        if (isNormalInteger($("#input3").val()) != false)
            $("#input3subdiv").removeClass("has-error");
        else
            $("#input3subdiv").addClass("has-error");
    });


    $('#prevButton').unbind();
    $('#nextButton').unbind();
    $("#prevButton").attr("disabled", false);
    $("#nextButton").attr("disabled", false);
    $('#prevButton').click(function () {
        $("#nextButton").text(dict("NextButtonText"));
        GetStep4Items();
    });
    $("#nextButton").click(function () {
        //add to basket

        if ((isNormalInteger($("#input1").val()) == true) && (isNormalInteger($("#input2").val()) == true) && (isNormalInteger($("#input3").val()) == true))
        {
            var nbi = new basketItem();
            nbi.type = step1SelectedObj.name;
            nbi.color = step2SelectedObj.name;
            nbi.glass = step3SelectedObj.name;
            nbi.fitting = step4SelectedObj.name

            nbi.idtype = step1SelectedObj.e;
            nbi.idcolor = step2SelectedObj.id;
            nbi.idglass = step3SelectedObj.e;
            nbi.idfitting = step4SelectedObj.e;
            nbi.idconstruction = step5SelectedObj.id

            nbi.base64 = step5SelectedObj.base64;
            nbi.quantity = $("#input1").val();
            nbi.width = $("#input2").val();
            nbi.height = $("#input3").val();

            if ((nbi.height > 0) && (nbi.width > 0) && (nbi.quantity > 0))
                basketItems[basketItems.length] = nbi;
            else
                return;
            $("#nextButton").text(dict("NextButtonText"));
            if (basketItems.length == 0)
                $("#badge").text("");
            else
                $("#badge").text(basketItems.length);
            GetStep1Items();
        }
    });
}

function FormModal() {
    var modal = ""
    + "    <div id=\"myModal_OrderForm\" class=\"modal fade\" role=\"dialog\">"
    + "         <div class=\"modal-dialog\">"
    + "             <div class=\"modal-content\">"
    + "                 <div class=\"modal-header so-modal-header\">"
    + "                     <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>"
    + "                     <h4 class=\"modal-title\">" + dict("OrderFormTitle") + "</h4>"
    + "                 </div>"
    + "                 <div class=\"modal-body\">"
    + "                     <fieldset>"
    + "                         <div class=\"form-group\">"
    + "                             <label for=\"CustomerName\">" + dict("CustomerNameForm") + "</label>"
    + "                             <input type=\"text\" class=\"form-control\" id=\"CustomerName\">"
    + "                         </div>"
    + "                         <div class=\"form-group\">"
    + "                             <label for=\"surname\">" + dict("NameForm") + "</label>"
    + "                             <input type=\"text\" class=\"form-control\" id=\"surname\">"
    + "                         </div>"
    + "                         <div class=\"form-group\">"
    + "                             <label for=\"email\">" + dict("EmailForm") + "</label>"
    + "                             <input type=\"email\" class=\"form-control\" id=\"email\">"
    + "                         </div>"
    + "                         <div class=\"form-group\">"
    + "                             <label for=\"gsm\">" + dict("GSMForm") + "</label>"
    + "                             <input type=\"tel\" class=\"form-control\" id=\"gsm\">"
    + "                         </div>"
    + "                         <div class=\"form-group\">"
    + "                             <label for=\"street\">" + dict("StreetForm") + "</label>"
    + "                             <input type=\"text\" class=\"form-control\" id=\"street\">"
    + "                         </div>"
    + "                         <div class=\"form-group\">"
    + "                             <label for=\"postalcode\">" + dict("PostalCodeForm") + "</label>"
    + "                             <input type=\"text\" class=\"form-control\" id=\"postalcode\">"
    + "                         </div>"
    + "                         <div class=\"form-group\">"
    + "                             <label for=\"city\">" + dict("CityForm") + "</label>"
    + "                             <input type=\"text\" class=\"form-control\" id=\"city\">"
    + "                         </div>"
    + "                         <div class=\"form-group\">"
    + "                             <label for=\"note\">" + dict("NoteForm") + "</label>"
    + "                             <textarea class=\"form-control\" rows=\"5\" id=\"note\"></textarea>"
    + "                         </div>"
    + "                     </fieldset>"
    + "                     <div id=\"recaptcha2\"></div>"
    + "                 </div>"
    + "                 <div class=\"modal-footer\">"
    + "                     <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">" + dict("CloseButtonText") + "</button>"
    + "                     <button id=\"sendOffer\" type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">" + dict("SendOfferButtonText") + "</button>"
    + "                 </div>"
    + "             </div>"
    + "         </div>"
    + "    </div>"
    + "<script type=\"text/javascript\">"
    + "    var onloadCallback = function() {"
    + "        grecaptcha.render('recaptcha2', {"
    + "            'sitekey' : '" + reCAPTCHAKey + "',"
    + "            'theme' : 'light'"
    + "        });"
    + "    };"
    + "</script>";
    $("#orderForm").append(modal);


    $('#sendOffer').click(function (e) {

        var CustomerName = $("#CustomerName").val();
        var surname = $("#surname").val();
        var email = $("#email").val();
        var gsm = $("#gsm").val();
        var street = $("#street").val();
        var postalcode = $("#postalcode").val();
        var city = $("#city").val();
        var note = $("#note").val();

        var itemsToSend = JSON.stringify(basketItems);
        var captchaResponse = grecaptcha.getResponse();

        var url = fbURL + "/SendOrder";
        $.ajax({
            url: url,
            method: "POST",
            async: true,
            cache: false,
            crossDomain: true,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: "{ \"APIKey\": \"" + callerID + "\", \"keyID\": \"" + objID + "\", \"data\": "
            + "     { "
            + "          \"id\": \"" + guid() + "\", "
            + "         \"captcha\": \"" + captchaResponse + "\", "
            + "         \"Name\": \"" + CustomerName + "\", "
            + "         \"Surname\": \"" + surname + "\", "
            + "         \"Email\": \"" + email + "\", "
            + "         \"GSM\": \"" + gsm + "\", "
            + "         \"Street\": \"" + street + "\", "
            + "         \"PostalCode\": \"" + postalcode + "\", "
            + "         \"City\": \"" + city + "\", "
            + "         \"Note\": \"" + note + "\", "
            + "         \"items\": " + itemsToSend + " "
            + "     }"
            + "}"
        })
        .done(function (data) {
            var obj = data.d;


            //order send

            actualStep = 1;
            basketItems = [];
            $("#badge").text("");
            $("#nextbutton").attr("disabled", true);

            var infoBody = ""
            + "<div id=\"infotext\"><br/><div class=\"alert alert-success\">"
            + "    <strong>" + dict("MyBasketInfoHeaderText") + "</strong> " + dict("MyBasketSendText")
            + "</div></div>";
            $("#basket").empty();
            $("#basket").append(infoBody);
            $("#nextButton").hide();
        })
        .always(function () {

        });

    });
}


function basketItem() {
    this.id = guid();

    this.quantity = 0;
    this.type = "";
    this.color = "";
    this.glass = "";
    this.fitting = "";

    this.idtype = "";
    this.idcolor = "";
    this.idglass = "";
    this.idfitting = "";
    this.idconstruction = "";

    this.width = 0;
    this.height = 0;
    this.base64 = "";
    this.price = 0;
    this.currency = "";
    this.note = "";
}

/* ------------------------------------------------------------------------------------- */
/* Translation */
/* ------------------------------------------------------------------------------------- */